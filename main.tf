#Provide Required Module for Testing

# Setup for Terraform & AWS Provider
terraform {
  required_version = ">=0.14.5"
}

provider "aws" {
  region = "us-east-1"
  ignore_tags {
    key_prefixes = ["platform_"]
  }
}

# Source Code to test 

module "ses_setup" {

  source = "git@github.com:cloudbinary/terraform-aws-ses.git?ref=v0.10"

  # create ses domain identity
  create_ses_domain_identity = true
  ses_domain_identity        = "example.com"

  # create ses domain identity verification
  create_ses_domain_identity_verification = false

  # create ses_email_identity
  ses_email_identities = ["example@abc.com"]

  environment = "regression-testing"
}
